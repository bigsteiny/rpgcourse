﻿using UnityEngine;

public class CameraRaycaster : MonoBehaviour
{
    public Layer[] layerPriorities = {
        Layer.Enemy,
        Layer.Walkable        
    };

    float distanceToBackground = 100.0f;    //depth of field basically
    Camera viewCamera;

    RaycastHit m_hit;
    public RaycastHit hit
    {
        get { return m_hit; }
    }

    Layer m_layerHit;
    public Layer currentLayerHit
    {
        get { return m_layerHit; }
    }
   
    void Start() // TODO Awake?
    {
        viewCamera = GetComponent<Camera>();
        if (viewCamera == null)
            Debug.LogError("Camera is null for cameraRaycaster");
    }

    public delegate void OnLayerChange(Layer newLayerType);
    public event OnLayerChange layerChangeObservers;    //todo:: consider deregistering layer change observers on leaving the game or scene

    void Update()
    {
        
        // Look for and return priority layer hit
        foreach (Layer layer in layerPriorities)
        {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue)
            {
            //    Debug.Log("CameraRayCaster::Update()::HasValue: " + hit.Value.collider.ToString());
                bool didLayerTypeChange = m_layerHit != layer;                
                m_hit = hit.Value;
                m_layerHit = layer;
                if (didLayerTypeChange) //tell say the mouse cursor handler that we have actually changed the layer type we're pointing at.
                {                   
                    if (layerChangeObservers != null)
                        layerChangeObservers(layer);//callback, draw different cursor                                      
                    else
                    {
                      //  Debug.LogError("no layerChangeObservers target");
                    }
                }
                
                return;
            }
        }

        // Otherwise return background hit
        m_hit.distance = distanceToBackground;
        m_layerHit = Layer.RaycastEndStop;
    }

    RaycastHit? RaycastForLayer(Layer layer)
    {
        int layerMask = 1 << (int)layer; // See Unity docs for mask formation
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground, layerMask);
        if (hasHit)
        {
            return hit;
        }
        return null;
    }
}
