﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    [Tooltip("Number of minutes per second that pass, try 60")]
    [SerializeField] float minutesPerSecond = 60.0f;
    // Update is called once per frame
    void Update()
    {
        //float angleThisFrame = 1 * Time.deltaTime;// (Time.deltaTime / 360) * minutesPerSecond;
        float angleThisFrame = (Time.deltaTime / 360) * minutesPerSecond;

        transform.Rotate(angleThisFrame, 0.0f, 0.0f);
    }
}
