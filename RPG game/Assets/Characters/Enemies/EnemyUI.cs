﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUI : MonoBehaviour
{
    Camera _cameraToLookAt;
    // Start is called before the first frame update
    void Awake()
    {
        _cameraToLookAt = Camera.main;
    }
    
    void LateUpdate()
    {   
        
        
        //keeps the UI looking at the main player.
        //transform.LookAt(_cameraToLookAt.transform);
        //transform.rotation = Quaternion.LookRotation(_cameraToLookAt.transform.forward);

        //transform.rotation = _cameraToLookAt.transform.rotation;
        transform.LookAt(transform.position + _cameraToLookAt.transform.rotation * Vector3.back,
            _cameraToLookAt.transform.rotation * Vector3.up);
    }
}
