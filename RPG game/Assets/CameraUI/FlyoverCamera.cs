﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyoverCamera : MonoBehaviour
{
    [SerializeField] bool doFlyoverIntro = true;
    [SerializeField] Transform[] waypoints;

    // Start is called before the first frame update
    void Start()
    {
        if (doFlyoverIntro)
            Camera.SetupCurrent(Camera.allCameras[1]);
        else
            OnFlyoverFinished();
    }

    // Update is called once per frame
    void Update()
    {
      //  transform.LookAt(waypoints[0]);
      //  GetComponent<Camera>().transform.LookAt(waypoints[0]);
       // transform.forward += Vector3.forward * 10;

      //  Debug.Log("Waypoints 0 is " + waypoints[0]);
    }

    public void OnFlyoverFinished()
    {
        Debug.Log("Changing camera");
        //Camera.FindObjectOfType<Main>
        GetComponent<Camera>().enabled = false;
        Camera.SetupCurrent(Camera.allCameras[0]);
    }
}
