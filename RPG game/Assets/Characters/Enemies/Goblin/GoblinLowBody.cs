﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinLowBody : MonoBehaviour
{
    Fighter fighterParent;
    // Start is called before the first frame update
    void Start()
    {
        fighterParent = GetComponentInParent<Fighter>();
        Debug.Assert(fighterParent);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Callback from animation when we hit something else with our sword
    private void Hit()
    {
        fighterParent.Hit();
    }
}
