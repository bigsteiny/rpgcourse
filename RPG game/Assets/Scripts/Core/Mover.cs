﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class Mover : MonoBehaviour, IAction
{
    ActionScheduler actionScheduler; 
    NavMeshAgent _navMeshAgent;
    NavMeshObstacle _navMeshObstacle;   //myself as an obstacle, if i'm stationary
    Animator _animator;
    Health health;
    BattlefieldController _battleController;

    //Helping the AI attacker algorithm only look for nearby enemies
    public int GridX {  get { return _myGridX;  } }
    public int GridZ { get { return _myGridZ; } }
    int _myGridX = int.MinValue;
    int _myGridZ = int.MinValue;

    [SerializeField] TextMeshProUGUI _textHelper;
    // Start is called before the first frame update
    void Start()
    {
        UpdateGridXZCoordinatesOnBattlefieldCoordinator();
    }
    void Awake()
    {
        InitPointers();        
    }
    Ray _lastRay;
    // Update is called once per frame
    void Update()
    {
        UpdateGridXZCoordinatesOnBattlefieldCoordinator();

        if (_textHelper != null)
            _textHelper.text = _myGridX + ", " + _myGridZ;

        _navMeshAgent.enabled = !health.IsDead; //dont allow movement if we're dead
        if (health.IsDead)
            return;
        UpdateAnimator();
    }

    public void UpdateGridXZCoordinatesOnBattlefieldCoordinator()
    {
        int currentGridX, currentGridZ;
        GetGridXZ(out currentGridX, out currentGridZ);
        if (currentGridX != _myGridX || currentGridZ != _myGridZ)
        {
            //On changed grid that i'm in
            _battleController.SetNewGridPosition(this, _myGridX, _myGridZ, currentGridX, currentGridZ);
            _myGridX = currentGridX;
            _myGridZ = currentGridZ;
        }
    }

    public void GetGridXZ(out int x, out int z)
    {
        x = 0; z = 0;
        float metresPerGridSquare = 10.0f;
        x = Mathf.FloorToInt(transform.position.x / metresPerGridSquare);
        z = Mathf.FloorToInt(transform.position.z / metresPerGridSquare);
       // Debug.Log("My position is " + transform.position.x + "," + transform.position.z);
    }

    /// <summary>
    /// Will cancel any existing actions such as attacking/casting, and begin this new action right now
    /// </summary>
    /// <param name="destination"></param>
    public void StartMoveAction(Vector3 destination)
    {
        actionScheduler.StartAction(this);        //cancel fighting etc        
        SetNavMeshAgentDestination(destination);
        _navMeshAgent.isStopped = false;
    }
    /// <summary>
    /// Sets the navmesh agent destination
    /// </summary>
    /// <param name="destination"></param>
    public void SetNavMeshAgentDestination(Vector3 destination)
    {
        _navMeshAgent.enabled = true;
        _navMeshAgent.destination = destination;        
    }
    public void CancelAction()
    {
       // Debug.Log("Stopped");       
       if (_navMeshAgent.enabled)
        _navMeshAgent.isStopped = true;
    }
    
    void InitPointers()
    {
        _battleController = FindObjectOfType<BattlefieldController>();
        _navMeshObstacle = GetComponent<NavMeshObstacle>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        actionScheduler = GetComponent<ActionScheduler>();
        health = GetComponent<Health>();
        Debug.Assert(_battleController);
        Debug.Assert(actionScheduler);
        Debug.Assert(_navMeshAgent);
        Debug.Assert(health);
        _animator = GetComponent<Animator>();   //animator might be in children
        if (_animator == null)        
            _animator = GetComponentInChildren<Animator>();
        Debug.Assert(_animator);        
    }
    /// <summary>
    /// Sets the blend tree's speed based on the nav mesh velocity
    /// </summary>
    private void UpdateAnimator()
    {
        string STRING_FORWARD_SPEED = "forwardSpeed";
        bool hasTarget = _navMeshAgent.destination != null;
        if (!hasTarget)
        {
            _animator.SetFloat(STRING_FORWARD_SPEED, 0.0f);
            return;
        }    

        //has a target
        float distToTarget = Mathf.Abs((transform.position - _navMeshAgent.destination).magnitude);
        if (distToTarget > 1.9)
        {
            Knight_Toon_Animation_Fix_Hack(false);
            _navMeshAgent.enabled = true;
            if (_navMeshObstacle)
                _navMeshObstacle.enabled = false;
          //  transform.rotation.SetLookRotation(_navMeshAgent.destination);

            
            Vector3 navMeshVelocity = _navMeshAgent.velocity;
            //convert to local variable relative to character
            Vector3 localVelocity = transform.InverseTransformDirection(navMeshVelocity);//convert from global to local velocity
            float speed = localVelocity.z;  //how fast am i moving in a forward direction
         //   Debug.Log(gameObject + " moving to target at " + _navMeshAgent.destination + ", dist is " + distToTarget + ", set my speed to " + Mathf.Abs(speed));
            _animator.SetFloat(STRING_FORWARD_SPEED, Mathf.Abs(speed));            
        }
        else
        {
            _navMeshAgent.enabled = false;
            if (_navMeshObstacle)
                _navMeshObstacle.enabled = true;

            Knight_Toon_Animation_Fix_Hack(true);
           // Debug.Log("Rotating because we're close, " + gameObject.ToString());
            _animator.SetFloat(STRING_FORWARD_SPEED, Mathf.Abs(0.0f));
           

        }
    }
    /// <summary>
    /// The stupid asset for the knight toon bounces around when very close to the target, the navmeshagent doesn't
    /// cope with it properly, so disable the agent if we're close.
    /// </summary>
    /// <param name="isCloseToTargetSoDisableNavMeshAgent"></param>
    private void Knight_Toon_Animation_Fix_Hack(bool isCloseToTargetSoDisableNavMeshAgent)
    {
        KnightToon toon = GetComponent<KnightToon>();
        if (toon && _navMeshAgent.enabled)        
            toon.GetComponent<NavMeshAgent>().isStopped = isCloseToTargetSoDisableNavMeshAgent;        
    }
    private void OnDrawGizmos()
    {
        //called every time gizmos are drawn, every frame
        Gizmos.color = Color.red;
        if (_navMeshAgent == null)
            return;
        if (GetComponent<KnightToon>() == null)
            Gizmos.color = Color.blue;
        Gizmos.DrawLine(this.transform.position, _navMeshAgent.destination);
    }
}
