﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class Health : MonoBehaviour
{
    [SerializeField] float hitPoints = 20.0f;   //starting hit points
    [SerializeField] float maxHitPoints = 30.0f;
    [SerializeField] Animator animator;
    public float healthAsPercentage { get { return (hitPoints / maxHitPoints) * 100.0f; } }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public bool IsDead {  get { return _isDead;  } }
    bool _isDead = false;
    public void OnTakeHit(float amount)
    {
        if (_isDead)
            return;
        
        hitPoints -= amount;
        if (hitPoints < 0.0)
            hitPoints = 0.0f;
        if (hitPoints <= 0.0f)
        {
            Die();
            return;
        }

        ShowHitAnimation();
      //  print("my new health is " + hitPoints);
    }
    private void ShowHitAnimation()
    {
        string DO_RECEIVED_HIT_ANIM_NAME = "DoReceivedHit";
        animator.SetTrigger(DO_RECEIVED_HIT_ANIM_NAME);
    }
    private void Die()
    {
        _isDead = true;
        GetComponent<ActionScheduler>().CancelCurrentAction();
        string DO_DEATH_TRIGGER_NAME = "DoDeathAnim";
        animator.SetTrigger(DO_DEATH_TRIGGER_NAME);
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

        //We are no longer an obstacle so remove our nav mesh obstacles so others can step over our corpse
        var navMeshObstable = GetComponent<NavMeshObstacle>();
        if (navMeshObstable != null)        
            navMeshObstable.enabled = false;

        var canvasForHealthbar = GetComponentInChildren<Canvas>();
        if (canvasForHealthbar != null)
            canvasForHealthbar.enabled = false;
    }
}
