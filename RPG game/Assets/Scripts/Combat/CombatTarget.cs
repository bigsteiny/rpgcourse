﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Something that can be hit and has a health
/// </summary>
public class CombatTarget : MonoBehaviour
{
    Health _myHealth;    
   // Fighter _target = null;

    // Start is called before the first frame update
    void Start()
    {
        _myHealth = GetComponent<Health>();
        if (_myHealth == null)
            Debug.LogError("_myHealth is null");        
    }

    public bool CanBeAttacked()
    {
        if (_myHealth == null)
            return false;
        return (!_myHealth.IsDead);
    }
    // Update is called once per frame
   // float timeSinceLastSphereCast = 0.0f;
  //  float timeBetweenSphereCasts = 1.0f;
    void Update()
    {
       // Debug.Log("I am a " + gameObject + ", my transform is " + transform.position + "/" + transform.rotation + ", my parent is " + transform.parent.ToString() + " at " + transform.parent.position);
        //Debug.Log(transform.gameObject + " facing: " + _target + ", WorldPos: " + transform.TransformPoint(transform.position).ToString());

        //Debug.DrawLine(transform.position, _target.transform.position, Color.green);
        //transform.Rotate(0.0f, 15.0f, 0.0f, Space.World);
        //Vector3 directionToWorldCoord = 
        //Debug.DrawLine(transform.position, transform.TransformDirection(_target.transform.position - transform.position), Color.cyan);
        //return;


      //  _agent.enabled = false;
      //  _agent.updateRotation = false;
        //transform.parent.rotation = Quaternion.LookRotation(_agent.velocity.normalized);


      /*  if (_myHealth.IsDead)
        {         
            _target = null;
            return;
        }*/
/*
        if (timeSinceLastSphereCast >= timeBetweenSphereCasts)
        {
            if (!_myHealth.IsDead)
                RaycastSphereForNearbyEnemy();
            timeSinceLastSphereCast = 0.0f;
        }
        else
            timeSinceLastSphereCast += Time.deltaTime;

        FaceTarget(_agent.destination);*/
    }
  /*
    private void RaycastSphereForNearbyEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 15.0f, Physics.AllLayers, QueryTriggerInteraction.Collide);
        if (hitColliders == null || hitColliders.Length == 0)
            return;
    
        foreach (var collider in hitColliders)
        {
            if (collider.GetComponentInParent<Fighter>() != null)
            {
               // Debug.Log(gameObject.ToString() + " found a nearby fighter collider object, " + hitColliders[0].gameObject.ToString());
                _target = collider.GetComponentInParent<Fighter>();                                                
                _agent.destination = _target.transform.position;               
            }
        }
    }
    */
   
}
