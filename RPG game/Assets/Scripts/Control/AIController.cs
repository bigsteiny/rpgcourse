﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class AIController : MonoBehaviour
{
    protected GameObject playerChar;
    protected GameObject preferredTarget;
    Health _myHealth;
    NavMeshAgent _agent;

    Vector3 guardLocation;

    float timeSinceLastSawPlayer = Mathf.Infinity;
    [SerializeField] float suspicionTime = 3f;
    [SerializeField] PatrolPath patrolPath;

    public virtual void Start()
    {
        playerChar = GameObject.FindWithTag("Player");
        _agent = GetComponent<NavMeshAgent>();
        if (_agent == null)
            Debug.LogError("agent is null");
        _myHealth = GetComponent<Health>();
        if (_myHealth == null)
            Debug.LogError("_myHealth is null");
        guardLocation = transform.position;
        _fighter = GetComponent<Fighter>();
    }

    public virtual void FindNewEnemyToAttack()
    {

    }

    Fighter _fighter;
    public void LateUpdate()
    {
        if (!IamAlive())
            return;
        
        if (preferredTarget == null || !_fighter.CanAttackTarget(preferredTarget.gameObject))
        {
            //I'm alive but my target isn't? find a new target!
            FindNewEnemyToAttack();
            if (preferredTarget != null)
            {
                transform.LookAt(preferredTarget.transform);
            }
            else
            {
                //nobody left to attack! 
            }
            //return; //questionable... should w ejust 'return' here? wont work if there is no enemies in the world?
        }
        if (preferredTarget != null)
            transform.LookAt(preferredTarget.transform);
        if (IsWithinChaseDistance(preferredTarget) && IamAlive() && _fighter.CanAttackTarget(preferredTarget.gameObject))
        {
            AttackBehaviour();
            timeSinceLastSawPlayer = 0.0f;
        }
        else if (timeSinceLastSawPlayer < suspicionTime)
        {   //We are suspicious!
            SuspiciousBehaviour();
        }
        else
        {            
            PatrolBehaviour();
        }
        timeSinceLastSawPlayer += Time.deltaTime;
    }

    
    private void SuspiciousBehaviour()
    {
        ActionScheduler scheduler = GetComponent<ActionScheduler>();
        scheduler.CancelCurrentAction();
    }

    private void AttackBehaviour()
    {        
        GetComponent<Fighter>().Attack(preferredTarget.GetComponent<CombatTarget>());
    }

    [SerializeField] float ChaseDistance = 1.0f;
    public bool IsWithinChaseDistance(GameObject target)
    {
        if (target == null)
            return false;

        double distance = Vector3.Distance(gameObject.transform.position, target.transform.position);
        return distance < ChaseDistance;
    }
    public bool IamAlive() { return !_myHealth.IsDead; }

    int currentWaypointIndex = 0;
    [SerializeField] float waypointTolerance = 3.0f;
    public void PatrolBehaviour()
    {
        Vector3 nextPosition = guardLocation;   //default, starting point.

        if (patrolPath != null) //if we're a patroller
        {
            if (AtWaypoint())
            {
                timeSpentAtWaypoint += Time.deltaTime;
                if (timeSpentAtWaypoint > dwellAtWaypointTime)
                {                    
                    CycleWaypoint();
                    timeSpentAtWaypoint = 0.0f;
                }
                print("Time at waypoint: " + timeSpentAtWaypoint);
            }
            nextPosition = GetCurrentWaypoint();
        }

        Mover mover = GetComponent<Mover>();
      //  print(gameObject.name + " at " + transform.position + ", patrolling to " + nextPosition);
        mover.StartMoveAction(nextPosition);
    }

    private Vector3 GetCurrentWaypoint()
    {
        return patrolPath.GetWaypoint(currentWaypointIndex).position;
    }

    /// <summary>
    /// Move to the next waypoint (or go back to the first, 0, waypoint)
    /// </summary>
    private void CycleWaypoint()
    {        
        if (currentWaypointIndex < patrolPath.GetWaypointCount() - 1)
            currentWaypointIndex++;
        else
            currentWaypointIndex = 0;
        timeSpentAtWaypoint = 0.0f;
        print(gameObject.name + "::CycleWaypoint to #" + currentWaypointIndex);
    }


    [SerializeField] float dwellAtWaypointTime = 4.0f;
    float timeSpentAtWaypoint = 0;
    /// <summary>
    /// Am i at my destination waypoint?
    /// </summary>
    /// <returns></returns>
    private bool AtWaypoint()
    {             
        Vector3 waypointLocation = patrolPath.GetWaypoint(currentWaypointIndex).position;
        var distanceToWaypoint = Vector3.Distance(transform.position, waypointLocation);
        print(gameObject.name + " waypoint is at " + transform.position + ", patrolling to " + waypointLocation + " (" + distanceToWaypoint.ToString("F1") + "m away)");
        return distanceToWaypoint <= waypointTolerance;
    }
    
    private void OnDrawGizmos()
    {
        //called every time gizmos are drawn, every frame        
        //// Gizmos.DrawLine(transform.position, currentClickTarget);
        Gizmos.color = Color.yellow;
        if (GetComponent<KnightToon>() == null)
            Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, ChaseDistance);
      
        //Vector3 nextPosition = GetCurrentWaypoint();

       // Gizmos.DrawSphere(nextPosition, 2.0f);

        //draw our walk stop radius        
        /*   Gizmos.color = Color.black;
           Gizmos.DrawSphere(currentDestination, 0.1f);

           //  Vector3 locationToStopIfEnemyWasClicked = ShortDestination(currentClickTarget, attackMoveStopRadius);
           Gizmos.color = Color.red;
           Gizmos.DrawSphere(currentDestination, 0.1f);

           //Draw attack sphere
           Gizmos.color = new Color(0.0f, 125.0f, 125.0f, 0.25f);
           Gizmos.DrawSphere(transform.position, attackMoveStopRadius);*/

    }
}
