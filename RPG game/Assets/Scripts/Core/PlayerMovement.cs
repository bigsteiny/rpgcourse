using System;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof (ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour
{
    ThirdPersonCharacter m_Character;   // A reference to the ThirdPersonCharacter on the object
    CameraRaycaster cameraRaycaster;
    Vector3 currentClickTarget; //where we clicked
    Vector3 currentDestination;        //where we're actually walking to, based off the initial Click
    bool m_Jump = false;
    [SerializeField] float walkMoveStopRadius = 0.2f;   //20cm, so if get to within 20cm of destination, stop moving.
    [SerializeField] float attackMoveStopRadius = 5.0f;   //20cm, so if get to within 20cm of destination, stop moving.

    private void Start()
    {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
       m_Character = GetComponent<ThirdPersonCharacter>();
        currentClickTarget = transform.position;
        currentDestination = transform.position;
    }

    //Todo:: update the WSAD being broken, and increase player speed?
    private void Update()
    {
        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
    }

    // Fixed update is called in sync with physics
    private bool ProcessWasdInput()
    {
        bool isWasdBeingUsed = false;

        // read inputs for wasd
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");       
        bool crouch = Input.GetKey(KeyCode.C);
        Vector3 camForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 wasdMove = v * camForward + h * Camera.main.transform.right;       
       
#if !MOBILE_INPUT
        // walk speed multiplier
        if (Input.GetKey(KeyCode.LeftShift)) wasdMove *= 0.5f;
#endif

        if (h != 0.0f || v != 0.0f || crouch)
            isWasdBeingUsed = true;
        
        if ((h != 0.0f || v != 0.0f) || crouch)
        {
            m_Character.Move(wasdMove, crouch, m_Jump);        // pass all parameters to the character control script        
            m_Jump = false;
        }

        return isWasdBeingUsed;
    }


    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        bool isWasdInputObserved = ProcessWasdInput();
        if (isWasdInputObserved)
        {
            Debug.Log("Wasd used, ignoring mouse click");
            currentClickTarget = transform.position;
            currentDestination = currentClickTarget;
            return;
        }

        if (Input.GetMouseButton(0))
        {
            print("Cursor raycast hit" + cameraRaycaster.hit.collider.gameObject.name.ToString());
            print("Clicked " + cameraRaycaster.currentLayerHit);
            currentClickTarget = cameraRaycaster.hit.point;  // So not set in default case
            switch (cameraRaycaster.currentLayerHit)
            {
                case Layer.Walkable:                                        
                    currentDestination = ShortDestination(cameraRaycaster.hit.point, walkMoveStopRadius);
                    print("New destination set to " + currentDestination);
                    break;
                case Layer.Enemy:
                    print("Clicked on enemy at " + cameraRaycaster.hit);                    
                    currentDestination = ShortDestination(cameraRaycaster.hit.point, attackMoveStopRadius);
                    print("New destination set to " + currentDestination);
                    break;
                default:
                    print("Unexpected layer found on click");
                    return;
            }
        }

        //using mouse click      
        Vector3 amountToMove = currentDestination - transform.position;
       // Debug.Log("Amount to move: " + amountToMove.magnitude);
        if (amountToMove.magnitude > walkMoveStopRadius)
        {
            //Debug.Log("moving by " + amountToMove.magnitude); //todo:: deal with the walk stop distance having to be 1.2 metres, just because of the walk speed causing dancing at the destination
            m_Character.Move(amountToMove, false, false);      //dont jump, dont crouch                
        }
        else
        {
            //Debug.Log("not moving");            
            m_Character.Move(Vector3.zero, false, false);
        }
    }

    private void OnDrawGizmos()
    {
        //called every time gizmos are drawn, every frame
        Gizmos.color = Color.black;        
        Gizmos.DrawLine(transform.position, currentClickTarget);
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(currentClickTarget, 0.2f);

        //draw our walk stop radius        
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(currentDestination, 0.1f);

      //  Vector3 locationToStopIfEnemyWasClicked = ShortDestination(currentClickTarget, attackMoveStopRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(currentDestination, 0.1f);

        //Draw attack sphere
        Gizmos.color = new Color(0.0f, 125.0f, 125.0f, 0.25f);
        Gizmos.DrawSphere(transform.position, attackMoveStopRadius);
        
    }
    /// <summary>
    /// Takes in an input vector, say a destination, and gives back a vector that is x amount short of that destination point
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="shortenAmount"></param>
    /// <returns></returns>
    private Vector3 ShortDestination(Vector3 destination, float shortenAmount)
    {
        var shortenedVector = (destination - transform.position).normalized * shortenAmount;
        return destination - shortenedVector;
    }
}

