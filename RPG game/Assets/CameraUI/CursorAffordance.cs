﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorAffordance : MonoBehaviour
{
    [SerializeField] CameraRaycaster cameraRayCaster;
    [SerializeField] Texture2D cursorWalk;
    [SerializeField] Texture2D cursorAttack;
    [SerializeField] Texture2D cursorUnknown;
    [SerializeField] Vector2 hotspotOffset = new Vector2(0, 0);

    private void Awake()
    {
        cameraRayCaster.layerChangeObservers += new CameraRaycaster.OnLayerChange(LayerPointingAtChanged);    //update the mouse cursor icon
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void LayerPointingAtChanged(Layer newLayerType)
    {
      //  print("New layer pointing at" + newLayerType);
        switch (newLayerType)
        {
            case Layer.Walkable:
                Cursor.SetCursor(cursorWalk, hotspotOffset, CursorMode.Auto);
                break;
            case Layer.Enemy:
                Cursor.SetCursor(cursorAttack, hotspotOffset, CursorMode.Auto);
                break;
            case Layer.RaycastEndStop:
                Cursor.SetCursor(cursorUnknown, hotspotOffset, CursorMode.Auto);
                //  Debug.LogError("Dont know what to show for mouse cursor");
                break;
            default:
                Debug.LogError("Unknown what we are casting to");
                break;
        }
    }
    // Update is called once per frame
    void LateUpdate()
    {
      
    }
}
