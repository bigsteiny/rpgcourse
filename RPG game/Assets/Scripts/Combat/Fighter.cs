﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fighter : MonoBehaviour, IAction
{

    [SerializeField] float weaponRange = 10.0f;
    [SerializeField] float timeBetweenAttacks = 5.0f;
    [SerializeField] AudioClip punchSound;
    AudioSource voice;
    float timeSinceLastAttack = float.MinValue;
    CombatTarget _combatTarget;
    ActionScheduler actionScheduler;

    public void Start()
    {
        voice = GetComponent<AudioSource>();
        actionScheduler = GetComponent<ActionScheduler>();
        if (actionScheduler == null)
            Debug.LogError("ActionScheduler was null");        
    }

    public void Attack(CombatTarget target)
    {
       // print("take that " + target.gameObject.ToString());
        _combatTarget = target;
        actionScheduler.StartAction(this);
    }
    public void CancelAction()
    {
       // print("Stopping attack (CancelAction)");
        _combatTarget = null;
    }

    
    private void Update()
    {
        timeSinceLastAttack += Time.deltaTime;
        if (_combatTarget == null)
            return;
        
        //Move towards the target if we're not within range.
        //float rangeToTarget = (transform.position - target.transform.position).magnitude;
        float rangeToTarget = Vector3.Distance(transform.position, _combatTarget.transform.position);       
        if (rangeToTarget > weaponRange)
        {
            //Debug.Log(gameObject.ToString() + "'s range to target " + _combatTarget.name + " is " + rangeToTarget.ToString("F2") + ", weapon range is " + weaponRange + " and so moving towards it");
            GetComponentInParent<Mover>().SetNavMeshAgentDestination(_combatTarget.transform.position);            
        }
        else
        {
            // Debug.Log("Range to target is " + rangeToTarget.ToString("F2") + ", weapon range is " + weaponRange + " and so stopping");            
            GetComponentInParent<Mover>().CancelAction();//stop any movement            
            if (CanAttackTarget(_combatTarget.gameObject))
                AttackBehaviour();
        }
    }
    public bool CanAttackTarget(GameObject target)
    {
        if (target == null)
            return false;
        return !target.GetComponent<Health>().IsDead;
    }

    private void AttackBehaviour()
    {
        transform.LookAt(_combatTarget.transform);        
        //DO the punch animation
        if (timeSinceLastAttack == float.MinValue || timeSinceLastAttack >= timeBetweenAttacks)
        {
            timeSinceLastAttack = 0.0f;
            const string TRIGGER_NAME_FOR_ANIM_EVENT__DO_ATTACK = "DoAttackAnim";
            Animator animator = GetComponent<Animator>();
            if (animator == null)                            
                animator = GetComponentInChildren<Animator>();  //for goblins the animator is in the child            
            animator.SetTrigger(TRIGGER_NAME_FOR_ANIM_EVENT__DO_ATTACK);
        }
    }

    [SerializeField] float WeaponDamage = 1.0f;
    /// <summary>
    /// Animation Event callback when a punch lands, automated by the animator for the 
    /// exact frame where a 'Hit' occurs
    /// </summary>
    public void Hit()
    {
       // print("Our punch landed");
        if (_combatTarget == null)
            return; //sholdn't happen?

        PlayPunchSound();

        Health healthComponent = _combatTarget.GetComponent<Health>();
        if (healthComponent == null)
        {
            Debug.LogError("Target didn't have any health");
        }
        healthComponent.OnTakeHit(WeaponDamage);
    }
    public void PlayPunchSound()
    {        
        if (voice == null)
            Debug.LogError("Voice is empty");
        voice.clip = punchSound;
        voice.Play();
        
    }
}
