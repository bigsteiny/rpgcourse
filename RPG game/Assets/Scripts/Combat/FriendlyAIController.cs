﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FriendlyAIController : AIController
{
    public override void Start()
    {
        base.Start();
        FindNewEnemyToAttack();
    }
    EnemyAIController GetClosestAliveEnemyLocation()
    {
        var ctrl = FindObjectOfType<BattlefieldController>();
        if (gameObject.name == "GoblinB (18)")
        {
            int b = 0;
        }
        var closestEnemy = ctrl.GetClosestEnemyAiControllerTo(transform.position);
        if (closestEnemy != null)
        {
            Debug.Log(gameObject.name + " found nearby enemy " + closestEnemy.gameObject.name + " at " + closestEnemy.transform.position.x + ", " + closestEnemy.transform.position.z);
        }
        else
        {
            Debug.Log(gameObject.name + " couldn't find any enemies...");
        }
        return closestEnemy;
    }
    /*
    EnemyAIController GetClosestAliveEnemyLocation()
    {
        EnemyAIController[] goblins = FindObjectsOfType<EnemyAIController>();
        float closestGoblinDistance = float.PositiveInfinity;
        int indexOfClosestGoblin = int.MaxValue;
        for (int i = 0; i <= goblins.GetUpperBound(0); i++)
        {            
            Health health = goblins[i].GetComponent<Health>(); //only look at alive targets
            if (health != null && !health.IsDead)
            {
                float distanceToGoblin = Vector3.Distance(goblins[i].transform.position, transform.position);
                if (Mathf.Abs(distanceToGoblin) < closestGoblinDistance)
                {
                    closestGoblinDistance = Mathf.Abs(distanceToGoblin);
                    indexOfClosestGoblin = i;
                }
            }
        }
        if (indexOfClosestGoblin != int.MaxValue)
            return goblins[indexOfClosestGoblin];
        else
            return null;
    }*/
    public override void FindNewEnemyToAttack()
    {
        var closestEnemy = GetClosestAliveEnemyLocation();
        if (closestEnemy != null)
            preferredTarget = closestEnemy.gameObject;   //friendly prefer the closest Enemy as their main target
        else
            preferredTarget = null;
    }
}
