﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Mover mover;
    // Start is called before the first frame update
    void Start()
    {
        mover = GetComponent<Mover>();
        if (mover == null)
            Debug.LogError("Mover is null");
    }
       
    // Update is called once per frame
    void Update()
    {
        if (InteractWithCombat())   //prioritize combat, if we're attacking... don't change our move target (again)
            return;

        if (InteractWithMovement())
            return;

    //    print("Nothing to do.");
    }
    private bool InteractWithCombat()
    {
        RaycastHit[] allItemsHit = Physics.RaycastAll(GetMouseRay());
        Fighter fighter = GetComponent<Fighter>();
        foreach (RaycastHit hit in allItemsHit)
        {
            CombatTarget target = hit.transform.GetComponentInParent<CombatTarget>();
            //Debug.Log("Hit at least something with a CombatTarget in the raycast");
            //Found something to attack!
            if (target == null)
                continue;
            if (target.gameObject == gameObject)    //dont punch yourself!
                continue;

            if (!fighter.CanAttackTarget(target.gameObject))
                continue;

            if (Input.GetMouseButtonDown(0))         //user clicking mouse down?                
                fighter.Attack(target);                             
            return true;    //we're hovering on something that can be attacked!
        }
        return false;
    }
   
    private bool InteractWithMovement()
    {
        RaycastHit _raycastHitObject;
        
        bool hasHit = Physics.Raycast(GetMouseRay(), out _raycastHitObject);
        if (hasHit)
        {
            if (Input.GetMouseButtonUp(0))
            {
                print("PlayerControlller::InteractWithMovement_GetMouseButton_Cancelling fighter's current target");
                Fighter pFighter = GetComponentInParent<Fighter>(); //if we were already attacking, make sure we're not anymore
                if (pFighter)
                    pFighter.CancelAction();

                mover.SetNavMeshAgentDestination(_raycastHitObject.point);                                
            }
            return true;            
        }
        return false;
    }

    private static Ray GetMouseRay()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition);
    }
}
