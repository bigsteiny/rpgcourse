﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BattlefieldController : MonoBehaviour
{
    int _nbrGridXSquares = 0;
    int _nbrGridZSquares = 0;
    float metresPerGridSquare = 10.0f;    
    List<EnemyAIController>[,] _enemyGridLocations;
    List<FriendlyAIController>[,] _friendlyGridLocations;
    // Start is called before the first frame update
    void Awake()
    {
        InitialiseGridCacheArrays();
    }    
    public bool IsInitialised { get; set; }

    public void InitialiseGridCacheArrays()
    {
        if (IsInitialised) //singleton
            return;
        Terrain terrain = FindObjectOfType<Terrain>();
        TerrainData terrainData = terrain.terrainData;
        _nbrGridXSquares = Mathf.FloorToInt(terrainData.size.x / metresPerGridSquare);
        _nbrGridZSquares = Mathf.FloorToInt(terrainData.size.z / metresPerGridSquare);
        Debug.Log("Terrain is " + _nbrGridXSquares + "x" + _nbrGridZSquares + " gridsquares");

        //prep up the list of enemies, capacity, 10x10
        _enemyGridLocations = new List<EnemyAIController>[_nbrGridXSquares, _nbrGridZSquares];
        _friendlyGridLocations = new List<FriendlyAIController>[_nbrGridXSquares, _nbrGridZSquares];
        for (int x = 0; x < _nbrGridXSquares; x++)
        {
            for (int z = 0; z < _nbrGridZSquares; z++)
            {
                _enemyGridLocations[x, z] = new List<EnemyAIController>();
                _friendlyGridLocations[x, z] = new List<FriendlyAIController>();
            }
        }
        IsInitialised = true;
        //Force all Mover objects in the game to update their grid locations and tell us where they are
        foreach (var mover in FindObjectsOfType<Mover>())
        {
            mover.UpdateGridXZCoordinatesOnBattlefieldCoordinator();
        }        
    }

    /// <summary>
    /// Sets a Mover object to exist in one of the grid lists, used for optimising nearby combatants
    /// </summary>
    /// <param name="mover">used to get the reference to the object maintained in the list</param>
    /// <param name="previousGridX">which X list does the mover currently reside in?</param>
    /// <param name="previousGridZ"></param>
    /// <param name="newGridX">which X list should the mover now reside in?</param>
    /// <param name="newGridZ"></param>
    public void SetNewGridPosition(Mover mover, int previousGridX, int previousGridZ, int newGridX, int newGridZ)
    {
        if (!IsInitialised)
            return;

        lock (_enemyGridLocations)
        {
            lock (_friendlyGridLocations)
            {
              //  Debug.Log(mover.gameObject.name + " changed grid to " + newGridX + ", " + newGridZ + " (from " + previousGridX + ", " + previousGridZ + ")");
                bool isEnemy = mover.GetComponentInParent<EnemyAIController>() != null;
                if (previousGridX == int.MinValue)  //first time inserting ourselves onto the battlefield
                {
                    if (isEnemy)
                    {
                        _enemyGridLocations[newGridX, newGridZ].Add(mover.GetComponent<EnemyAIController>());
                    }
                    else
                    {
                        //todo:: playerChar isn't added because it doesn't have a friendly AI controller object. for now.
                        if (mover.GetComponent<FriendlyAIController>())
                            _friendlyGridLocations[newGridX, newGridZ].Add(mover.GetComponent<FriendlyAIController>());
                    }
                    return;
                }

                if (isEnemy)
                {
                    EnemyAIController aiController = mover.GetComponentInParent<EnemyAIController>();
                    for (int ctrUnit = 0; ctrUnit < _enemyGridLocations[previousGridX, previousGridZ].Count; ctrUnit++)
                    {
                        if (_enemyGridLocations[previousGridX, previousGridZ][ctrUnit].Equals(aiController))
                        {   //found ourselves inthe list, pop us
                            _enemyGridLocations[previousGridX, previousGridZ].RemoveAt(ctrUnit);
                            //now push ourselves on the new list
                            _enemyGridLocations[newGridX, newGridZ].Add(aiController);
                            return;
                        }
                    }
                }
                else
                {
                    FriendlyAIController aiController = mover.GetComponentInParent<FriendlyAIController>();
                    for (int ctrUnit = 0; ctrUnit < _friendlyGridLocations[previousGridX, previousGridZ].Count; ctrUnit++)
                    {
                        if (_friendlyGridLocations[previousGridX, previousGridZ][ctrUnit].Equals(aiController))
                        {   //found ourselves inthe list, pop us
                            _friendlyGridLocations[previousGridX, previousGridZ].RemoveAt(ctrUnit);
                            //now push ourselves on the new list
                            _friendlyGridLocations[newGridX, newGridZ].Add(aiController);
                            return;
                        }
                    }
                }
            }//lock friendly
        }//lock enemies
    }

    int _efficientMatchesMade = 0;
    int _inefficientMatchesMade = 0;
    /// <summary>
    /// Finds the closest FriendlyAIController object to the specified location.
    /// First determines the 'grid' that the location corresponds to, and therefore
    /// the List of AIControllers that are adjacent and parses those first so its faster.
    /// Failing finding any candidates, just gets a list of ALL FriendlyAIControllers. Slower, but last resort.
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    public FriendlyAIController GetClosestFriendlyAiControllerTo(Vector3 location)
    {
        Debug.Log("Efficient matches: " + _efficientMatchesMade + ", inefficient: " + _inefficientMatchesMade);
        if (!IsInitialised)
            return null;
        int gridX, gridZ;        
        gridX = Mathf.FloorToInt(location.x / metresPerGridSquare);
        gridZ = Mathf.FloorToInt(location.z / metresPerGridSquare);
        lock (_friendlyGridLocations)
        {            
            float closestCombatant = float.PositiveInfinity;
            FriendlyAIController closestEnemySoFar = null;
            for (int ctrX = Mathf.Max(0, gridX - 1); ctrX <= Mathf.Min(_nbrGridXSquares - 1, gridX + 1); ctrX++)
            {   //check left, above, and right of us
                for (int ctrZ = Mathf.Max(0, gridZ - 1); ctrZ <= Mathf.Min(_nbrGridZSquares - 1, gridZ + 1); ctrZ++)
                {
                    //check above, aligned, and below us
                    List<FriendlyAIController> gridList = _friendlyGridLocations[ctrX, ctrZ];
                    if (gridList == null)
                    {
                        int b = 0;
                    }
                    foreach (var candidateCombatTarget in gridList)
                    {
                        if (!candidateCombatTarget.GetComponent<Health>().IsDead)
                        {
                            float distanceToCombatant = Vector3.Distance(candidateCombatTarget.transform.position, location);
                            if (Mathf.Abs(distanceToCombatant) < closestCombatant)
                            {
                                closestCombatant = Mathf.Abs(distanceToCombatant);
                                closestEnemySoFar = candidateCombatTarget;
                            }
                        }
                    }
                }
            }

            if (closestEnemySoFar == null)
            {   //nobody in any of the adjacent grids to us!! 
                FriendlyAIController[] candidateEnemies = FindObjectsOfType<FriendlyAIController>();
                closestCombatant = float.PositiveInfinity;
                int indexOfClosestCombatant = int.MaxValue;
                for (int i = 0; i <= candidateEnemies.GetUpperBound(0); i++)
                {
                    Health health = candidateEnemies[i].GetComponent<Health>(); //only look at alive targets
                    if (health != null && !health.IsDead)
                    {
                        float distanceToCombatant = Vector3.Distance(candidateEnemies[i].transform.position, location);
                        if (Mathf.Abs(distanceToCombatant) < closestCombatant)
                        {
                            closestCombatant = Mathf.Abs(distanceToCombatant);
                            indexOfClosestCombatant = i;
                        }
                    }
                }
                if (indexOfClosestCombatant != int.MaxValue)
                {
                    _inefficientMatchesMade++;
                   // Debug.Log("Couldn't find anyone near me, so resorted to a longer distance enemy of " + candidateEnemies[indexOfClosestCombatant].gameObject.name);
                    return candidateEnemies[indexOfClosestCombatant];
                }
                else
                {
                    Debug.Log("Game over, nobody left on the battlefield?");
                    return null;    //nobody on the battlefield at all
                }
            }

            _efficientMatchesMade++;
           // Debug.Log("Found a convenient enemy nearby of " + closestEnemySoFar.gameObject.name);
            return closestEnemySoFar;
        }
    }

    /// <summary>
    /// Finds the closest FriendlyAIController object to the specified location.
    /// First determines the 'grid' that the location corresponds to, and therefore
    /// the List of AIControllers that are adjacent and parses those first so its faster.
    /// Failing finding any candidates, just gets a list of ALL FriendlyAIControllers. Slower, but last resort.
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    public EnemyAIController GetClosestEnemyAiControllerTo(Vector3 location)
    {
        Debug.Log("Efficient matches: " + _efficientMatchesMade + ", inefficient: " + _inefficientMatchesMade);
        if (!IsInitialised)
            return null;
        int gridX, gridZ;
        gridX = Mathf.FloorToInt(location.x / metresPerGridSquare);
        gridZ = Mathf.FloorToInt(location.z / metresPerGridSquare);
        lock (_enemyGridLocations)
        {
            float closestCombatant = float.PositiveInfinity;
            EnemyAIController closestEnemySoFar = null;
            for (int ctrX = Mathf.Max(0, gridX - 1); ctrX <= Mathf.Min(_nbrGridXSquares - 1, gridX + 1); ctrX++)
            {   //check left, above, and right of us
                for (int ctrZ = Mathf.Max(0, gridZ - 1); ctrZ <= Mathf.Min(_nbrGridZSquares - 1, gridZ + 1); ctrZ++)
                {
                    //check above, aligned, and below us
                    List<EnemyAIController> gridList = _enemyGridLocations[ctrX, ctrZ];
                    if (gridList == null)
                    {
                        int b = 0;
                    }
                    foreach (var candidateCombatTarget in gridList)
                    {
                        if (!candidateCombatTarget.GetComponent<Health>().IsDead)
                        {
                            float distanceToCombatant = Vector3.Distance(candidateCombatTarget.transform.position, location);
                            if (Mathf.Abs(distanceToCombatant) < closestCombatant)
                            {
                                closestCombatant = Mathf.Abs(distanceToCombatant);
                                closestEnemySoFar = candidateCombatTarget;
                            }
                        }
                    }
                }
            }

            if (closestEnemySoFar == null)
            {   //nobody in any of the adjacent grids to us!! 
                EnemyAIController[] candidateEnemies = FindObjectsOfType<EnemyAIController>();
                closestCombatant = float.PositiveInfinity;
                int indexOfClosestCombatant = int.MaxValue;
                for (int i = 0; i <= candidateEnemies.GetUpperBound(0); i++)
                {
                    Health health = candidateEnemies[i].GetComponent<Health>(); //only look at alive targets
                    if (health != null && !health.IsDead)
                    {
                        float distanceToCombatant = Vector3.Distance(candidateEnemies[i].transform.position, location);
                        if (Mathf.Abs(distanceToCombatant) < closestCombatant)
                        {
                            closestCombatant = Mathf.Abs(distanceToCombatant);
                            indexOfClosestCombatant = i;
                        }
                    }
                }
                if (indexOfClosestCombatant != int.MaxValue)
                {
                    _inefficientMatchesMade++;
                    // Debug.Log("Couldn't find anyone near me, so resorted to a longer distance enemy of " + candidateEnemies[indexOfClosestCombatant].gameObject.name);
                    return candidateEnemies[indexOfClosestCombatant];
                }
                else
                {
                    Debug.Log("Game over, nobody left on the battlefield?");
                    return null;    //nobody on the battlefield at all
                }
            }

            _efficientMatchesMade++;
            // Debug.Log("Found a convenient enemy nearby of " + closestEnemySoFar.gameObject.name);
            return closestEnemySoFar;
        }
    }
}
