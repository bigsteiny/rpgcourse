﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class EnemyHealthBar : MonoBehaviour
{

    RawImage healthBarRawImage;
    [SerializeField] Health objectWithHitpoints;

    // Use this for initialization
    void Awake()
    {   
        healthBarRawImage = GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {        
        float xValue = -(0.5f * ((100 - (objectWithHitpoints.healthAsPercentage)) / 100.0f));
        healthBarRawImage.uvRect = new Rect(xValue, 0f, 0.5f, 1f);
     }
}

