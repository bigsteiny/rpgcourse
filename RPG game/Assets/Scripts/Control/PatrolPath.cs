﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPath : MonoBehaviour
{
    //[SerializeField] Transform[] waypoints;

    [SerializeField] float gizmoWaypointRadius = 1.0f;
    private void OnDrawGizmos()
    {
        //Transform[] waypoints
        for (int ctrWaypoint = 0; ctrWaypoint < transform.childCount-1; ctrWaypoint++)
        {
            Transform a = transform.GetChild(ctrWaypoint);
            Transform b = transform.GetChild(ctrWaypoint+1);
            Gizmos.color = Color.red;
            //draw connecting line
            Gizmos.DrawLine(a.position, b.position);
            //draw points
            Gizmos.DrawSphere(a.position, gizmoWaypointRadius);
            Gizmos.DrawSphere(b.position, gizmoWaypointRadius);
        }        
    }

    public Transform GetWaypoint(int index)
    {
        return transform.GetChild(index);
    }
    public Transform GetNextWaypoint(int index)
    {
        if (index == transform.childCount - 1)
            return GetWaypoint(0);  //back to the first waypoint
        return GetWaypoint(index+1);
    }
    public int GetWaypointCount()
    {
        return transform.childCount;
    }
}
