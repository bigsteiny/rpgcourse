﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    //Vector3 _cameraOffset;
    GameObject m_player;

    // Start is called before the first frame update
    void Start()
    {
        m_player = GameObject.FindGameObjectWithTag("Player");
        //_cameraOffset = gameObject.transform.position - m_player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 newPosition = m_player.transform.position;        
        transform.position = newPosition;
    }
}
