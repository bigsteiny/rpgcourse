﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionScheduler : MonoBehaviour
{
    IAction _currentAction;

    public void StartAction(IAction newAction)
    {
        if (_currentAction == newAction)    //same action, maybe just different location etc, dont cancel it.
            return;
        
        if (_currentAction != null)         //different action type, so cancel the previous one
        {
         //   print("ActionScheduler cancelling existing action of type " + _currentAction);
            _currentAction.CancelAction();
        }        
        _currentAction = newAction;               
    }
    public void CancelCurrentAction()
    {
        StartAction(null);
    }
}
